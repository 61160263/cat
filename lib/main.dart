import 'package:flutter/material.dart';
import 'dart:async';

import 'cat.dart';
import 'cat_dao.dart';

void main() async {

  var Haru = Cat(id: 0, name: 'Haru', age: 60);
  var Shiro = Cat(id: 0, name: 'Shiro', age: 50);
  await CatDao.insertCat(Haru);
  await CatDao.insertCat(Shiro);

  print(await CatDao.cats());

  Haru = Cat(
    id: Haru.id,
    name: Haru.name,
    age: Haru.age + 7,
  );

  await CatDao.updateCat(Haru);
  print(await CatDao.cats());

  await CatDao.deleteCat(0);
  print(await CatDao.cats());
}